from selenium import webdriver
import requests
from lxml import html as LxmlDriver
import logging
import time
import os
from settings import *
from selenium.common.exceptions import ElementNotVisibleException
from selenium.webdriver.common.keys import Keys
class baseBrowser(object):

    @classmethod
    def initiate_browser(self, browser_name):
        if browser_name != "Lxml":
                options = webdriver.ChromeOptions()
                options.add_argument('--headless')
                options.add_argument('--no-sandbox')
                driver = webdriver.Chrome(executable_path=BROWSER_PATH, chrome_options=options)
        else:
            driver = None
        return driver

    @classmethod
    def load_url(self, url, browser_reffernce):

        browser_reffernce.get(url)
        time.sleep(3)
        return browser_reffernce

    @classmethod
    def extract_elements(self, element, selector , multiple):
        if multiple is True:
            selected_ele = element.find_elements_by_css_selector(selector)
        else:
            selected_ele = element.find_element_by_css_selector(selector)
        return selected_ele




    @classmethod
    def click_action(self, element, browser_reff, click_type, css, wait=None):
        if wait is None:
                if click_type != "js":
                    element.click()
                    time.sleep(1)
                else:
                    browser_reff.execute_script("document.querySelector('{}').click()".format(css))
                return True

    @classmethod
    def extract_attribute(self, selected_ele,attribute_name,multiple):
        if multiple is False:
            element =  selected_ele.get_attribute(attribute_name)
        else:
            element = list()
            for ele in selected_ele:
                element.append(ele.get_attribute(attribute_name))

        return element



    @classmethod
    def send_keys(cls,selected_ele,string_sent):
        selected_ele.send_keys(string_sent)
        selected_ele.send_keys(Keys.ENTER)



