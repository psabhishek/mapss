
import re


class Library(object):
    @classmethod
    def join(self, input_list, delemiter):
        if isinstance(input_list, list):
            return delemiter.join(input_list).encode("utf-8")
        return input_list

    @classmethod
    def strip_element(cls, output):
        if isinstance(output, str):
            return_ele = output.strip()
            return return_ele
        elif isinstance(output, unicode):
            return_ele = str(output).strip()
            return return_ele
        elif isinstance(output, list):
            return_ele = []
            for ele in output:
                return_ele.append(ele.strip())
            return return_ele

    @classmethod
    def strip_html(cls, raw_html):
        if isinstance(raw_html, list):  # Do a recursive call if its a list
            return list(map(lambda f: cls.strip_html(f), raw_html))
        cleanr = re.compile('<.*?>')
        cleantext = re.sub(cleanr, '', raw_html)
        return cleantext

    @classmethod
    def strip_index(cls, raw_input, metadata):
        if isinstance(metadata, list):
            if raw_input:
                input_list = raw_input.split(metadata[0])
                return input_list[metadata[1]]
        return raw_input