from settings import *

robot_config = {
    "browser": "CHROME",
    "start_uri": STARTING_URL,
    "steps": [
        {
            "dataExtraction": {
                "step": "search_string",
                "query": "input[autofocus='autofocus']",
                "search_string": SEARCH_STRING,
                "multiple": False,
                "element": "ELEMENT",
            }
        },

        {
            "dataExtraction": {
                "step": "extract_element",
                "query": "h3.section-result-title ",
                "multiple": True,
                "next_step": [
                    {
                        "extract": {
                            "query": ".section-listbox .section-hero-header img",
                            "multiple": False,
                            "element": "src",
                            "type": "image"
                        }
                    },
                    {
                        "extract": {
                            "query": ".section-review-review-content>span[class]",
                            "multiple": True,
                            "element": "innerHTML",
                            "type": "review"

                        }
                    }, {
                        "extract": {
                            "query": "div[data-tooltip='Copy address'] .section-info-text span.widget-pane-link",
                            "multiple": False,
                            "element": "innerHTML",
                            "type": "address"
                        }
                    },
                    {
                        "extract": {
                            "query": ".section-info-hour-text span",
                            "multiple": True,
                            "element": "innerHTML",
                            "type": "open_till"

                        }
                    },
                    {
                        "extract": {
                            "query": "div[data-tooltip='Open website'] span.widget-pane-link",
                            "multiple": False,
                            "element": "innerHTML",
                            "type": "website"

                        }
                    },
                    {
                        "extract": {
                            "query": "div[data-tooltip='Copy phone number'] span.widget-pane-link",
                            "multiple": False,
                            "element": "innerHTML",
                            "type": "Phone Number"

                        }
                    },
{
                        "extract": {
                            "query": "h1.section-hero-header-title",
                            "multiple": False,
                            "element": "innerHTML",
                            "type": "location"

                        }
                    },
                    {
                        "goback": {
                            "query": ".section-listbox.section-listbox-root>button",
                            "multiple": False,

                        }
                    }
                ]
            },
        },

    ]
}
