from browser import baseBrowser
import csv
import json
from robot_config import robot_config
from settings import *
import time
class Extractor():
    
    def __init__(self,robot_config):
        self.robot_config = robot_config
        
        
    def start_browser(self):
        self.browser_reff = baseBrowser.initiate_browser(browser_name= self.robot_config["browser"])
        
    def load_uri(self):
        baseBrowser.load_url(url= self.robot_config["start_uri"],browser_reffernce= self.browser_reff)


    def extraction(self):
        element = self.robot_config["steps"]
        for ele in element:
            extractor = Baseextractor(ele,self.browser_reff)
            data = extractor.data_extracton()
            if data is True:
                pass
            else:
                f = csv.writer(open(SEARCH_STRING+"".csv"", "wb+"))
                f.writerow(["website", "open_till", "url", "image", "Phone Number","lat","long","location","address","review"])
                for data in data:
                    f.writerow([ele["website"],
                                ele["open_till"],
                                ele["url"],
                                ele["image"],
                                ele["Phone Number"],
                                ele["lat/long"][0],
                                ele["lat/long"][1],
                                ele["location"],ele["address"],ele["review"]
                                ])

    def execute(self):
        self.start_browser()
        self.load_uri()
        self.extraction()

        import pdb;pdb.set_trace()


class Baseextractor():

    def __init__(self, element,browser_reff):
        self.browser = browser_reff
        if isinstance(element,dict):
            self.element = element
            self.dataextraction = self.element["dataExtraction"]
            self.mutiple = self.dataextraction["multiple"]
            self.query = self.dataextraction["query"]
            self.out_data = []


    def data_extracton(self):
        if isinstance(self.dataextraction,dict):
            if "search_string" ==  self.dataextraction["step"]:
                element = baseBrowser.extract_elements(self.browser,self.query,self.mutiple)
                baseBrowser.send_keys(element,SEARCH_STRING)
                time.sleep(5)
                return True
            elif "extract_element" == self.dataextraction["step"]:
                element = baseBrowser.extract_elements(self.browser, self.query, self.mutiple)

                if "next_step" in self.dataextraction.keys():
                    for idx,ele in enumerate(element):
                        # try:

                            out_data = {}
                            baseBrowser.click_action(element[idx],self.browser,"notjs",css=None)
                            time.sleep(5)
                            for nex in self.dataextraction["next_step"]:
                                if "goback" in  nex.keys():
                                    print ("url", self.browser.current_url)
                                    out_data["url"] =  self.browser.current_url
                                    print ("lat/long", self.browser.current_url.split("/@")[-1].split(",")[:2])
                                    out_data["lat/long"]= self.browser.current_url.split("/@")[-1].split(",")[:2]
                                    back = baseBrowser.extract_elements(self.browser,nex["goback"]["query"] , False)
                                    baseBrowser.click_action(back, self.browser, "notjs", css=None)
                                    element = baseBrowser.extract_elements(self.browser, self.query, self.mutiple)
                                else:
                                    try:
                                        data = baseBrowser.extract_elements(self.browser,nex["extract"]["query"] , nex["extract"]["multiple"])
                                        print (nex["extract"]["type"],baseBrowser.extract_attribute(data,nex["extract"]["element"],nex["extract"]["multiple"]))
                                        out_data[nex["extract"]["type"]]=baseBrowser.extract_attribute(data,nex["extract"]["element"],nex["extract"]["multiple"])
                                    except:
                                        print (nex["extract"]["type"],None)
                                        out_data[nex["extract"]["type"]]= None
                            print out_data
                            self.out_data.append(out_data)
                        # except:
                        #     import pdb;pdb.set_trace()
                return self.out_data







if __name__ == "__main__":
    exe = Extractor(robot_config)
    exe.execute()